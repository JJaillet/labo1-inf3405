import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Database {

	/**
	 * prend en entr� le nom d'utilisateur et le mot de passe et les contre v�rifie avec la database. 
	 * Dans le cas ou ce nom d'utilisateur est disponible, cr�e un utilisateur avec ce mot de passe. 
	 * Dans le cas ou ce nom d'utilisateur n'est plus disponible et que le nom de passe concorde, connecte l'utilisateur.
	 * Dans le cas ou ce nom d'utilisateur n'est plus disponible et que le nom de passe concorde pas, envoie un message d'erreur. 
	 * @param username de l'utilisateur
	 * @param password de l'utilisateur
	 * @return le message � afficher en fonction de si les credentials sont valides 
	 * @throws IOException 
	 */
	static String verifyCrendential(String username, String password) throws IOException{
		File yourFile = new File(globalVariable.CREDENTIALDB);
		yourFile.createNewFile();
		Scanner scanner = new Scanner( new File(globalVariable.CREDENTIALDB));
	    //now read the file line by line...
		String credential= "username:" + username + "," +"password:" + password;
	    int lineNum = 0;
	    while (scanner.hasNextLine()) {
	        String line = scanner.nextLine();
	        lineNum++;
	        String formatedusername = "username:" + username;
	        if(line.contains(formatedusername)) {
	        	if(line.contains("password:" + password)){
	        		return "connexion r�ussis";
	        	}else {
	        		return "Erreur dans la saisie du mot de passe";
	        	}
	        }
	    }
		writeToDB(globalVariable.CREDENTIALDB, credential);
    	return "votre compte � �t� cr��";
	}
	/** 
	 * fonction permettant d'�crire dans un fichier texte
	 * @param DB nom du fichier
	 * @param message � �crire dans le fichier
	 */
	 static void writeToDB(String DB, String message){
		try {
		      FileWriter myWriter = new FileWriter(DB, true);
		      myWriter.write(message + System.lineSeparator());
		   
		      myWriter.close();
		  } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		  }
		  
	}
	/**
	 * fonction r�cup�rant les 15 derniers messages envoy� dans le fichier text, 
	 * et retourne un array de string contenant les 15 derniers message en ordre. 
	 * @return un array string contenant les 15 derniers message envoy� en ordre
	 * @throws IOException 
	 */
	static String[] getLastMessage() throws IOException{
		File yourFile = new File(globalVariable.MESSAGEDB);
		yourFile.createNewFile();
		Scanner scanner = new Scanner( new File(globalVariable.MESSAGEDB));
		   int lineNum = 0;
		   String[] lastmessage = new String[15];
		   String[] returnmessage = new String[15];
		    while (scanner.hasNextLine()) {
		        lastmessage[lineNum%15]= scanner.nextLine();
		        lineNum++;
		    }
		    Boolean moreThan15 = lineNum >= 15;
		    int numOfMessage = moreThan15 ? 15 : lineNum; // si nombre de ligne sup�rieur
		    if(!moreThan15){
		    	return lastmessage;
		    }else {
		    	for (int i=0; i < numOfMessage; i++){
			    	returnmessage[i] = lastmessage[lineNum%15];
			    	lineNum++;
			    }
		    }
		    
		        return returnmessage;
	}
}
