import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.Socket;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
public class ClientHandler extends Thread {
		private Socket socket;
		private int clientNumber;
		private DataOutputStream out;
		private DataInputStream in; 
		boolean login = false;
		public ClientHandler(Socket socket, int clientNumber) throws IOException {
			this.socket= socket; 
			this.out = new DataOutputStream (socket.getOutputStream());
			this.in = new DataInputStream(socket.getInputStream());
			this.clientNumber = clientNumber;
			System.out.println("New connection with client#"  + clientNumber +" at " + socket);
		}
		public void run()
		{
			
		try {

		// Cr�ation d'un canal sortant pour envoyer des messages au client 
		out.writeUTF("Hello from server you are client#" + clientNumber);
		String username= "";
		while(true){
			String message = in.readUTF();
			if(login == false){
				username = message;
				String password = in.readUTF();
				String credentialResponse = Database.verifyCrendential(username, password); // v�rifie que les noms d'utilisateur concorde 
				if (!credentialResponse.equals("Erreur dans la saisie du mot de passe")){
					login = true;
					// sendToClient("loggedIN");
				}
				System.out.println(credentialResponse);
				sendToClient(credentialResponse);
				if(login){ // si la connexion est r�ussi, envoi les messages pr�c�dents 
					sendPreviousMessage();
				}
				
			}
			else{
				//System.out.println(message);
				//out.writeUTF(message);
				String formatedMessage =
						"[" + username + "- " +
						socket.getInetAddress() +
						":" + 
						socket.getPort() + 
						" - "+
						getCurrentLocalDateTimeStamp() + 
						"]: " + 
						message;
				Server.sendMessageToAll(formatedMessage,this);
				System.out.println(formatedMessage);
				Database.writeToDB(globalVariable.MESSAGEDB, formatedMessage);
			}
		}
		} catch (IOException e){ 
			//System.out.println("d�connection du client#" + clientNumber + ": " + e);// debug
		System.out.println("d�connection du client#" + clientNumber);
		}
		
		finally {
			try{
				// Fermeture de la connexion avec le client 
				socket.close();

			} catch (IOException e){

				System.out.println("Couldn't close a socket, what's going on?");

				System.out.println("Connection with clients + clienthumber 4 closed");
			}
		}
	}
		/**
		 * envoie un message au client et uniquement au client de ce clientHandler 
		 * @param message
		 * @throws IOException
		 */
	public void sendToClient(String message) throws IOException{
		out.writeUTF(message);
	}
	/**
	 * envoie les 15 dernier messages envoy�s au client et uniquement au client de ce clientHandler
	 * @throws IOException
	 */
	public void sendPreviousMessage() throws IOException{
		String[] lastMessage = Database.getLastMessage();
		int i=0;
		while( i < lastMessage.length && lastMessage[i] != null){
			sendToClient(lastMessage[i]);
			i++;
		}
	}
	
	/** 
	 * fonction retournant l'heure selon un format pr�cis
	 * // pris de : https://stackoverflow.com/questions/1459656/how-to-get-the-current-time-in-yyyy-mm-dd-hhmisec-millisecond-format-in-java
	 * @return un string de l'heure 
	 */
	public static String getCurrentLocalDateTimeStamp() {
	    return LocalDateTime.now()
	       .format(DateTimeFormatter.ofPattern("yyyy-MM-dd@HH:mm:ss.SSS"));
	}
		
}
