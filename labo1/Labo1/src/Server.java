import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.Vector;

public class Server {

	private static ServerSocket listener; 
	public static Vector<ClientHandler> listThreads = new Vector<ClientHandler>(); 
	public static void main(String[] args) throws IOException {
		int clientNumber = 0;
		Scanner keyboard = new Scanner(System.in);
		String serverAddress ="";
		int serverPort = 0;
		boolean ipAdressValid = false;
		boolean portValid = false;
		//redemande une addresse ip jusq'� ce que qu'il recevoit une adresse valide
		while(!ipAdressValid){
			System.out.println("Enter your server Address:");
			if(keyboard.hasNext()){
				serverAddress = keyboard.next();
				if(Client.verifyAddressForm(serverAddress)){	
					ipAdressValid=true;
				}else {
					System.out.println("entr� invalide");
				}
			}else {
				System.out.println("entr� invalide");
			}
		}
	
		//redemande un port jusqu'� ce qu'il recevoit un port valide
		while(!portValid){
			System.out.println("Enter port number:");
				if(keyboard.hasNext()){
					if(keyboard.hasNextInt()){
						serverPort =keyboard.nextInt();
					}else {
						String consumeEntry = keyboard.next(); // consume entry as it was not an int 
					}
					if(Client.verifyPort(serverPort)){
						portValid = true;
					}else {
						System.out.println("entr� invalide");
					}
				}else{
					
					System.out.println("entr� invalide");
				}
			}
		listener = new ServerSocket();
		listener.setReuseAddress(true);
		InetAddress serverIP = InetAddress.getByName(serverAddress);
			
		listener.bind(new InetSocketAddress(serverIP, serverPort));
		
		System.out.format("The server is running on %s:%d%n", serverAddress, serverPort);
		try {
			
			while(true){
				listThreads.add(new ClientHandler(listener.accept(), clientNumber++));
				listThreads.lastElement().start();
			}
		}
		finally {
			listener.close();
		}
	}
	/**
	 * envoie un message � tous les clients connecter, sauf le client venant d'envoyer le message 
	 * @param message: le message � envoy�
	 * @param sender: le clienthandler ayant envoy� le message
	 * @throws IOException
	 */
	public static void sendMessageToAll(String message, ClientHandler sender) throws IOException{
		for(ClientHandler i : listThreads){
			if(i != sender){
				i.sendToClient(message);
			}
		}
		
	}
}
